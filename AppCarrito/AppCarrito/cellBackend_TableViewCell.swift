//
//  cellBackend_TableViewCell.swift
//  AppCarrito
//
//  Created by ANDRES DE LA TORRE on 13/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class cellBackend_TableViewCell: UITableViewCell {

    @IBOutlet weak var nameHeroLabel: UILabel!
    
    @IBOutlet weak var heroImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
