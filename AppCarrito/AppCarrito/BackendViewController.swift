//
//  BackendViewController.swift
//  AppCarrito
//
//  Created by ANDRES DE LA TORRE on 13/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class BackendViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var backendHeroesListVIew: UITableView!
    let bm = BackEndManager()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return heroesArrayBackEnd.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellBackEnd", for: indexPath) as! cellBackend_TableViewCell
        
        cell.nameHeroLabel.text = heroesArrayBackEnd[indexPath.row].nombre!.uppercased().replacingOccurrences(of: "_", with: " ")
        
        cell.heroImageView.image = heroesArrayBackEnd[indexPath.row].img!
        
        return cell
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector:#selector(cargarHeroes), name: NSNotification.Name("actualizarBackEnd"), object: nil)
       
        if heroesArrayBackEnd.count == 0 {
            bm.getHeroesFromBackEnd()

        }
        

        // Do any additional setup after loading the view.
    }
    
    func cargarHeroes() {
        backendHeroesListVIew.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var celdaSeleccionada:Int?
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        celdaSeleccionada = indexPath.row
        return indexPath

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinoController = segue.destination as! InfoBackEndViewController
        destinoController.index = celdaSeleccionada!

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
