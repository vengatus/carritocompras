//
//  cellAPI_TableViewCell.swift
//  AppCarrito
//
//  Created by Juan Erazo on 12/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class cellAPI_TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var nameHeroLabel: UILabel!
    
    @IBOutlet weak var heroImageView: UIImageView!
    
    var heroe:SHeroe!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillData(){
        nameHeroLabel.text = heroe.nombre!.uppercased().replacingOccurrences(of: "_", with: " ")
        
        if heroe.img == nil {
            let bm = BackEndManager()
            bm.getHeroeImage(heroe.nombre!, completionHandler: { (imageR) in
                DispatchQueue.main.async {
                    self.heroImageView.image  = imageR
//                    heroesArray[heroe.id + 1].img = imageR
                    self.heroe.img = imageR
                }
            })
        } else {
            heroImageView.image = heroe.img
        }
    }

}
