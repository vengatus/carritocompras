//
//  DotaHeroesViewController.swift
//  AppCarrito
//
//  Created by Juan Erazo on 12/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class DotaHeroesViewController: UIViewController,UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var apiTableView: UITableView!
    
    let bm = BackEndManager()
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return heroesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellAPI = tableView.dequeueReusableCell(withIdentifier: "cellAPI", for: indexPath) as! cellAPI_TableViewCell
        cellAPI.heroe = heroesArray[indexPath.row]
        cellAPI.fillData()
        return cellAPI
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bm.getHeroes()
        
        NotificationCenter.default.addObserver(self, selector:#selector(cargarHeroes), name: NSNotification.Name("actualizar"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    func cargarHeroes() {
        apiTableView.reloadData()
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    var celdaSeleccionada:Int?
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        celdaSeleccionada = indexPath.row
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinoController = segue.destination as! InfoViewController
        destinoController.index = celdaSeleccionada!
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
