//
//  GlobalVar.swift
//  AppCarrito
//
//  Created by Juan Erazo on 12/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import Foundation
import UIKit

var heroesArray = [SHeroe]()

var heroesArrayBackEnd = [HeroeBackEnd]()


class HeroeBackEnd {
    var idAPI:Int?
    var nombre:String?
    var img:UIImage?
    
    init(idAPI:Int,nombre:String,img:UIImage){
        self.idAPI = idAPI
        self.nombre = nombre
        self.img = img
    }
}
