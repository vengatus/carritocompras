//
//  InfoViewController.swift
//  AppCarrito
//
//  Created by Juan Erazo on 14/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var imgInfo: UIImageView!
    var index:Int = 0
    let bm = BackEndManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector:#selector(notificar), name: NSNotification.Name("notificacion"), object: nil)


        // Do any additional setup after loading the view.
    }
    
    func notificar(_ notificacion:Notification){
         let caso=notificacion.userInfo?["case"] as! Bool
        var alert:UIAlertController
        if caso == false {
            alert = UIAlertController(title: "Error", message: "Ese heroe ya existe", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))

           
        } else {
            alert = UIAlertController(title: "Éxito", message: "Se ha agregado con exito", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: nil))
        }
        
  
         self.present(alert,animated:true,completion:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func agregarBackEndPressed(_ sender: Any) {

        bm.addHeroToBackEnd(heroesArray[index])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        idLabel.text =  "\(heroesArray[index].id!   )"
        let name = heroesArray[index].nombre
        nombreLabel.text = name!.uppercased().replacingOccurrences(of: "_", with: " ")
        imgInfo.image = heroesArray[index].img
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
