//
//  BackEndManager.swift
//  AppCarrito
//
//  Created by Juan Erazo on 12/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper


class BackEndManager {
    
    
    func getHeroes() {
        let url = "https://api.steampowered.com/IEconDOTA2_570/GetHeroes/v0001/?key=3F8783E282D470C5FCD45C2DF88A8220"
        
        Alamofire.request(url).responseObject { (response: DataResponse<SHeroesApiResponse>) in
            
            let heroesResponse = response.result.value
        
            if let heroesArr = heroesResponse?.resultado {
                heroesArray = heroesArr.heroes!
                 NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object: nil)
            }
        }
    }
    
    func getHeroeImage(_ name:String,completionHandler: @escaping(UIImage)->()){
        let url = "http://cdn.dota2.com/apps/dota2/images/heroes/\(name)_lg.png"
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
    }
    
    func addHeroToBackEnd(_ heroe:SHeroe)  {
        let url = "http://localhost:1337/Heroe/create?idAPI=\(heroe.id!)&nombre=\(heroe.nombre!)&rutaImagen=http://cdn.dota2.com/apps/dota2/images/heroes/\(heroe.nombre!)_lg.png"

        
        Alamofire.request(url).validate().responseJSON { response in
            switch response.result {
            case .success:
                if heroesArrayBackEnd.count == 0 {
                    self.getHeroesFromBackEnd()
                } else {
                    heroesArrayBackEnd.append(HeroeBackEnd(idAPI: heroe.id!, nombre: heroe.nombre!, img: heroe.img!))
                    NotificationCenter.default.post(name: NSNotification.Name("actualizarBackEnd"), object: nil)
                }
                
                NotificationCenter.default.post(name: NSNotification.Name("notificacion"), object: nil,userInfo:["case":true])
              
            case .failure:
                NotificationCenter.default.post(name: NSNotification.Name("notificacion"), object: nil,userInfo:["case":false])
                

            }
        }
        
        
    }
    
    func getHeroesFromBackEnd(){
        Alamofire.request("http://localhost:1337/Heroe/find").responseJSON { response in
            
            if let JSON = response.result.value {
                
                let arrayJSON = JSON as! NSArray;
                
                for i in arrayJSON {
                    let dict = i as! NSDictionary
                    let id = dict["idAPI"]! as! Int
                    let nombre = dict["nombre"]! as! String
                    let ruta = dict["rutaImagen"]! as! String
                    Alamofire.request(ruta).responseImage { response in
                        guard let image = response.result.value else {
                            return
                        }
                        heroesArrayBackEnd.append(HeroeBackEnd(idAPI: id, nombre: nombre, img: image))
                        NotificationCenter.default.post(name: NSNotification.Name("actualizarBackEnd"), object: nil)
  
                    }
                }
                
            }
            
        }
    }
    
    
    
}
