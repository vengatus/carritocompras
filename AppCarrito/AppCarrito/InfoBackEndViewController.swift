//
//  InfoBackEndViewController.swift
//  AppCarrito
//
//  Created by Juan Erazo on 15/6/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class InfoBackEndViewController: UIViewController {
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var imgInfo: UIImageView!
    
    var index:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        idLabel.text =  "\(heroesArrayBackEnd[index].idAPI!   )"
        nombreLabel.text = heroesArrayBackEnd[index].nombre!.uppercased().replacingOccurrences(of: "_", with: " ")
        imgInfo.image = heroesArrayBackEnd[index].img

    }

    
}
